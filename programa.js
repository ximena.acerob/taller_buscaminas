//Clase
class Campo {

  unaCasilla;

  iniciarJuego() {

    //Variables
    let i;
    let divTablero = document.getElementById("divTablero");
  
    for (i = 1; i < 65; i++) {
      //Crear un objeto de tipo input
      this.unaCasilla = document.createElement("input");

      //Agregar algunos atributos al objeto
      this.unaCasilla.type = "button";
      this.unaCasilla.value = " ";
      this.unaCasilla.setAttribute("id", "casilla" + i);
      this.unaCasilla.setAttribute("class", "casilla");
      this.unaCasilla.setAttribute("onClick", "miTablero.ocultar(this.id)");
    
      divTablero.appendChild(this.unaCasilla);
      if (i % 8 === 0) {
        let salto = document.createElement("br");
        divTablero.appendChild(salto);
      }
      if (this.unaCasilla.value == 1){
        this.unaCasilla.setAttribute("ShowHidenElement");
      }
  }
}

  ocultar(id) {
  let miCasilla = document.getElementById(id);
  miCasilla.setAttribute("style", "visibility: hidden");
  }
}
 let miTablero = new Campo();